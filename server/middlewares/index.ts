import { injectResponseHandler } from './injectResponseHandler';

export default () => {
  return [injectResponseHandler()];
};
