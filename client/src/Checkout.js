import { loadScript as loadPaypal } from "@paypal/paypal-js";
import {
  Card,
  Col,
  Divider,
  Row,
  Space,
  Spin,
  Typography,
  message,
} from "antd";
import { useEffect, useRef, useState } from "react";

function ItemDetail({ item }) {
  return (
    <div
      style={{
        display: "flex",
        color: "#fff",
        backgroundColor: "transparent",
        justifyContent: "space-between",
      }}
    >
      <div>
        {item.name} ({item.code})
      </div>
      <div>USD {item.price}</div>
    </div>
  );
}

function ItemTotal({ selectedItems }) {
  return (
    <div
      style={{
        display: "flex",
        color: "#fff",
        backgroundColor: "transparent",
        justifyContent: "space-between",
      }}
    >
      <Typography.Title style={{ color: "#fff" }} level={3}>
        Total
      </Typography.Title>
      <Typography.Title style={{ color: "#fff" }} level={3}>
        USD {selectedItems.reduce((total, item) => total + Number(item.price), 0)}
      </Typography.Title>
    </div>
  );
}

function Checkout({ selectedItems = [], setSelectedItems }) {
  const paypalDivRef = useRef(null);
  const [paypalClientId, setPaypalClientId] = useState();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      await fetch(`${process.env.REACT_APP_API_HOST}/paypal/clientid`, {
        headers: {
          "x-api-key": process.env.REACT_APP_API_KEY,
        },
      })
        .then((res) => {
          if (res.ok) {
            return res.json();
          }
          throw res;
        })
        .then((jsonData) => setPaypalClientId(jsonData.data.clientId))
        .catch((err) => {
          console.error(err);
          if (err && err.statusCode === 404)
            message.error(`API does not exist`);
          message.error(`Code ${err?.statusCode}: ${err?.statusText}`);
        });

      setLoading(false);
    })();
  }, []);

  useEffect(() => {
    if (!paypalClientId) return;
    (async () => {
      const paypal = await loadPaypal({
        clientId: paypalClientId,
        disableFunding: "credit,card",
        currency: "USD",
      });

      await paypal
        .Buttons({
          style: {
            disableMaxWidth: true,
          },

          onClick: function (data) {
            if (selectedItems.length <= 0) {
              message.error(`You haven't selected an item`);
              // dont proceed createOrder when no selected an item
              return false;
            }
            return true;
          },
          createOrder: async function () {
            const intentOrderId = await fetch(
              `${process.env.REACT_APP_API_HOST}/paypal/intent`,
              {
                method: "POST",
                body: JSON.stringify({
                  ids: selectedItems.map((i) => i.id),
                }),
                headers: {
                  "x-api-key": process.env.REACT_APP_API_KEY,
                },
              }
            )
              .then((res) => {
                if (res.ok) {
                  return res.json();
                }
                throw res;
              })
              .then((jsonData) => jsonData.data.orderId)
              .catch(() => {});

            if (intentOrderId) {
              return intentOrderId;
            }
          },
          onApprove: async function (data, actions) {
            if (actions?.order)
              // Capture the funds from the transaction
              await actions.order.capture();
            setSelectedItems([])

            message.success(`Thank you for the purchase. We will email you the exam guideline soon.`)
          },
          onError: function (error) {
            message.error(error.message);
          },
          onCancel: () => {
            message.warning(`Order Cancelled`);
          },
        })

        .render(paypalDivRef.current);
    })();
    return () => {
      if (paypalDivRef.current) {
        paypalDivRef.current.innerHTML = "";
      }
    };
  }, [paypalClientId, selectedItems]);

  return (
    <Row
      style={{ width: "100%", height: "100%" }}
      justify="space-between"
      align="middle"
    >
      <Col style={{ width: "100%" }}>
        <Space direction="vertical" style={{ width: "80%" }}>
          <h4>Checkout</h4>
          <Card style={{ backgroundColor: "transparent" }}>
            {selectedItems.map((i, index) => (
              <ItemDetail item={i} />
            ))}
            {selectedItems.length === 0 && (
              <Typography.Text style={{ color: "#fff" }}>
                No item selected...
              </Typography.Text>
            )}
            <Divider style={{ backgroundColor: "#fff" }} />
            <ItemTotal selectedItems={selectedItems} />
          </Card>
          {!loading ? (
            <div ref={paypalDivRef} />
          ) : (
            <>
              <Spin size="large" />
              Loading paypal...
            </>
          )}
        </Space>
      </Col>
      <Col style={{ width: "100%" }}>
        <Typography.Text style={{ color: "#bbb" }}>
          &copy; 2023 CW All Rights Reserved
        </Typography.Text>
      </Col>
    </Row>
  );
}

export default Checkout;
