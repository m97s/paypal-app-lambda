import { useEffect, useState } from "react";
import {
  Card,
  Checkbox,
  Radio,
  Skeleton,
  Space,
  Spin,
  Tag,
  Typography,
  message,
} from "antd";

function CatalogDetail({ catalog }) {
  return (
    <Card style={{ width: "700px", backgroundColor: "transparent" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div style={{ textAlign: "start" }}>
          <Typography.Title style={{ color: "#fff" }} level={5}>
            {catalog.name} <Tag color="#0a0"> {catalog.provider} </Tag>
          </Typography.Title>
          <Typography.Text type="warning">
            Code: {catalog.code}
            <br /> Exam Date: {catalog.exam_date}
          </Typography.Text>
        </div>
        <div>
          <Typography.Title style={{ color: "red" }} level={3}>
            USD {catalog.price}
          </Typography.Title>
        </div>
      </div>
    </Card>
  );
}

function Catalog({ selectedItems=[], setSelectedItems }) {
  const [catalogs, setCatalogs] = useState([]);

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      await fetch(`${process.env.REACT_APP_API_HOST}/catalogs`, {
        method: "GET",
        headers: {
          "x-api-key": process.env.REACT_APP_API_KEY,
        },
      })
        .then((res) => {
          if (res.ok) {
            return res.json();
          }
          throw res;
        })
        .then((jsonData) => setCatalogs(jsonData.data.catalogs))
        .catch((err) => {
          console.error(err);
          if (err && err.status === 404) message.error(`API does not exist`);
          message.error(`Code ${err?.status}: ${err?.statusText}`);
        });

      setLoading(false);
    })();
  }, []);

  return (
    <div style={{ height: "100%" }}>
      <h4>Select your exam:</h4>
      {loading && <Spin size="large" />}
      <Skeleton loading={loading} active>
        <Checkbox.Group
          onChange={(ids) =>
            setSelectedItems(catalogs.filter((c) => ids.includes(c.id)))
          }
          value={selectedItems.map((i) => i.id)}
        >
          <Space direction="vertical" align="left">
            {catalogs.map((catalog) => (
              <Checkbox value={catalog.id}>
                <CatalogDetail catalog={catalog} />
              </Checkbox>
            ))}
          </Space>
        </Checkbox.Group>
        {catalogs.length === 0 && (
          <Card
            style={{
              backgroundColor: "transparent",
              width: "100%",
              height: "100%",
              color: "#bbb",
            }}
          >
            No exam available at the moment...
          </Card>
        )}
      </Skeleton>
    </div>
  );
}

export default Catalog;
